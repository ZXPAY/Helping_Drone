# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 17:00:16 2017

@author: zxpay
"""

import cv2
import numpy as np
import time

face_cascade = cv2.CascadeClassifier('haarcascade_fullbody.xml')
hog = cv2.HOGDescriptor()
hog.setSVMDetector( cv2.HOGDescriptor_getDefaultPeopleDetector())
cap = cv2.VideoCapture('walking.mp4')

while True:
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray,1.3,5)
    for (x,y,w,h) in faces:
        cv2.rectangle(img, (x,y), (x+w, y+h), (255,0,0), 2)

    cv2.imshow('img',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break


cap.release()
cv2.destroyAllWindows()



