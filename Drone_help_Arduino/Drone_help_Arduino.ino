/*
 * 空拍機 Code
 */
#include <Wire.h>  // I2C Library
#include <SPI.h>  // SPI 通訊Library
#include <Adafruit_Sensor.h>  // BMP280 氣壓感測器
#include <Adafruit_BMP280.h>  // BMP280 氣壓感測器
#include <PPMrcIn.h>  // Recieve PPM signal
#include <Statistic.h>  // Recieve PPM signal
#include <Servo.h>   // Brushless motor library

// MPU9250 參數設定 **********************************************************************************************************
float pitch, yaw, roll;
float init_pitch, init_yaw, init_roll;
float deltat = 0.0f, sum = 0.0f;        // integration interval for both filter schemes
// See also MPU-9250 Register Map and Descriptions, Revision 4.0, RM-MPU-9250A-00, Rev. 1.4, 9/9/2013 for registers not listed in 
// above document; the MPU9250 and MPU9150 are virtually identical but the latter has a different register map
//
//Magnetometer Registers
#define AK8963_ADDRESS   0x0C
#define AK8963_WHO_AM_I  0x00 // should return 0x48
#define AK8963_INFO      0x01
#define AK8963_ST1       0x02  // data ready status bit 0
#define AK8963_XOUT_L   0x03  // data
#define AK8963_XOUT_H  0x04
#define AK8963_YOUT_L  0x05
#define AK8963_YOUT_H  0x06
#define AK8963_ZOUT_L  0x07
#define AK8963_ZOUT_H  0x08
#define AK8963_ST2       0x09  // Data overflow bit 3 and data read error status bit 2
#define AK8963_CNTL      0x0A  // Power down (0000), single-measurement (0001), self-test (1000) and Fuse ROM (1111) modes on bits 3:0
#define AK8963_ASTC      0x0C  // Self test control
#define AK8963_I2CDIS    0x0F  // I2C disable
#define AK8963_ASAX      0x10  // Fuse ROM x-axis sensitivity adjustment value
#define AK8963_ASAY      0x11  // Fuse ROM y-axis sensitivity adjustment value
#define AK8963_ASAZ      0x12  // Fuse ROM z-axis sensitivity adjustment value

#define SELF_TEST_X_GYRO 0x00                  
#define SELF_TEST_Y_GYRO 0x01                                                                          
#define SELF_TEST_Z_GYRO 0x02

/*#define X_FINE_GAIN      0x03 // [7:0] fine gain
#define Y_FINE_GAIN      0x04
#define Z_FINE_GAIN      0x05
#define XA_OFFSET_H      0x06 // User-defined trim values for accelerometer
#define XA_OFFSET_L_TC   0x07
#define YA_OFFSET_H      0x08
#define YA_OFFSET_L_TC   0x09
#define ZA_OFFSET_H      0x0A
#define ZA_OFFSET_L_TC   0x0B */

#define SELF_TEST_X_ACCEL 0x0D
#define SELF_TEST_Y_ACCEL 0x0E    
#define SELF_TEST_Z_ACCEL 0x0F

#define SELF_TEST_A      0x10

#define XG_OFFSET_H      0x13  // User-defined trim values for gyroscope
#define XG_OFFSET_L      0x14
#define YG_OFFSET_H      0x15
#define YG_OFFSET_L      0x16
#define ZG_OFFSET_H      0x17
#define ZG_OFFSET_L      0x18
#define SMPLRT_DIV       0x19
#define CONFIG           0x1A
#define GYRO_CONFIG      0x1B
#define ACCEL_CONFIG     0x1C
#define ACCEL_CONFIG2    0x1D
#define LP_ACCEL_ODR     0x1E   
#define WOM_THR          0x1F   

#define MOT_DUR          0x20  // Duration counter threshold for motion interrupt generation, 1 kHz rate, LSB = 1 ms
#define ZMOT_THR         0x21  // Zero-motion detection threshold bits [7:0]
#define ZRMOT_DUR        0x22  // Duration counter threshold for zero motion interrupt generation, 16 Hz rate, LSB = 64 ms

#define FIFO_EN          0x23
#define I2C_MST_CTRL     0x24   
#define I2C_SLV0_ADDR    0x25
#define I2C_SLV0_REG     0x26
#define I2C_SLV0_CTRL    0x27
#define I2C_SLV1_ADDR    0x28
#define I2C_SLV1_REG     0x29
#define I2C_SLV1_CTRL    0x2A
#define I2C_SLV2_ADDR    0x2B
#define I2C_SLV2_REG     0x2C
#define I2C_SLV2_CTRL    0x2D
#define I2C_SLV3_ADDR    0x2E
#define I2C_SLV3_REG     0x2F
#define I2C_SLV3_CTRL    0x30
#define I2C_SLV4_ADDR    0x31
#define I2C_SLV4_REG     0x32
#define I2C_SLV4_DO      0x33
#define I2C_SLV4_CTRL    0x34
#define I2C_SLV4_DI      0x35
#define I2C_MST_STATUS   0x36
#define INT_PIN_CFG      0x37
#define INT_ENABLE       0x38
#define DMP_INT_STATUS   0x39  // Check DMP interrupt
#define INT_STATUS       0x3A
#define ACCEL_XOUT_H     0x3B
#define ACCEL_XOUT_L     0x3C
#define ACCEL_YOUT_H     0x3D
#define ACCEL_YOUT_L     0x3E
#define ACCEL_ZOUT_H     0x3F
#define ACCEL_ZOUT_L     0x40
#define TEMP_OUT_H       0x41
#define TEMP_OUT_L       0x42
#define GYRO_XOUT_H      0x43
#define GYRO_XOUT_L      0x44
#define GYRO_YOUT_H      0x45
#define GYRO_YOUT_L      0x46
#define GYRO_ZOUT_H      0x47
#define GYRO_ZOUT_L      0x48
#define EXT_SENS_DATA_00 0x49
#define EXT_SENS_DATA_01 0x4A
#define EXT_SENS_DATA_02 0x4B
#define EXT_SENS_DATA_03 0x4C
#define EXT_SENS_DATA_04 0x4D
#define EXT_SENS_DATA_05 0x4E
#define EXT_SENS_DATA_06 0x4F
#define EXT_SENS_DATA_07 0x50
#define EXT_SENS_DATA_08 0x51
#define EXT_SENS_DATA_09 0x52
#define EXT_SENS_DATA_10 0x53
#define EXT_SENS_DATA_11 0x54
#define EXT_SENS_DATA_12 0x55
#define EXT_SENS_DATA_13 0x56
#define EXT_SENS_DATA_14 0x57
#define EXT_SENS_DATA_15 0x58
#define EXT_SENS_DATA_16 0x59
#define EXT_SENS_DATA_17 0x5A
#define EXT_SENS_DATA_18 0x5B
#define EXT_SENS_DATA_19 0x5C
#define EXT_SENS_DATA_20 0x5D
#define EXT_SENS_DATA_21 0x5E
#define EXT_SENS_DATA_22 0x5F
#define EXT_SENS_DATA_23 0x60
#define MOT_DETECT_STATUS 0x61
#define I2C_SLV0_DO      0x63
#define I2C_SLV1_DO      0x64
#define I2C_SLV2_DO      0x65
#define I2C_SLV3_DO      0x66
#define I2C_MST_DELAY_CTRL 0x67
#define SIGNAL_PATH_RESET  0x68
#define MOT_DETECT_CTRL  0x69
#define USER_CTRL        0x6A  // Bit 7 enable DMP, bit 3 reset DMP
#define PWR_MGMT_1       0x6B // Device defaults to the SLEEP mode
#define PWR_MGMT_2       0x6C
#define DMP_BANK         0x6D  // Activates a specific bank in the DMP
#define DMP_RW_PNT       0x6E  // Set read/write pointer to a specific start address in specified DMP bank
#define DMP_REG          0x6F  // Register in DMP from which to read or to which to write
#define DMP_REG_1        0x70
#define DMP_REG_2        0x71 
#define FIFO_COUNTH      0x72
#define FIFO_COUNTL      0x73
#define FIFO_R_W         0x74
#define WHO_AM_I_MPU9250 0x75 // Should return 0x71
#define XA_OFFSET_H      0x77
#define XA_OFFSET_L      0x78
#define YA_OFFSET_H      0x7A
#define YA_OFFSET_L      0x7B
#define ZA_OFFSET_H      0x7D
#define ZA_OFFSET_L      0x7E

// Using the MSENSR-9250 breakout board, ADO is set to 0 
// Seven-bit device address is 110100 for ADO = 0 and 110101 for ADO = 1
#define ADO 1
#if ADO
#define MPU9250_ADDRESS 0x69  // Device address when ADO = 1
#else
#define MPU9250_ADDRESS 0x68  // Device address when ADO = 0
#define AK8963_ADDRESS 0x0C   //  Address of magnetometer
#endif  

#define AHRS true         // set to false for basic data read

// Set initial input parameters
enum Ascale {
  AFS_2G = 0,
  AFS_4G,
  AFS_8G,
  AFS_16G
};

enum Gscale {
  GFS_250DPS = 0,
  GFS_500DPS,
  GFS_1000DPS,
  GFS_2000DPS
};

enum Mscale {
  MFS_14BITS = 0, // 0.6 mG per LSB
  MFS_16BITS      // 0.15 mG per LSB
};

// Specify sensor full scale
uint8_t Gscale = GFS_250DPS;
uint8_t Ascale = AFS_2G;
uint8_t Mscale = MFS_16BITS; // Choose either 14-bit or 16-bit magnetometer resolution
uint8_t Mmode = 0x02;        // 2 for 8 Hz, 6 for 100 Hz continuous magnetometer data read
float aRes, gRes, mRes;      // scale resolutions per LSB for the sensors
  
// Pin definitions
//int intPin = 2;  // These can be changed, 2 and 3 are the Arduinos ext int pins
//int myLed = 13; // Set up pin 13 led for toggling

int16_t accelCount[3];  // Stores the 16-bit signed accelerometer sensor output
int16_t gyroCount[3];   // Stores the 16-bit signed gyro sensor output
int16_t magCount[3];    // Stores the 16-bit signed magnetometer sensor output
float magCalibration[3] = {0, 0, 0}, magbias[3] = {0, 0, 0};  // Factory mag calibration and mag bias
float gyroBias[3] = {0, 0, 0}, accelBias[3] = {0, 0, 0}, magScale[3]  = {0, 0, 0};      // Bias corrections for gyro and accelerometer
int16_t tempCount;      // temperature raw count output
float   temperature;    // Stores the real internal chip temperature in degrees Celsius
float   SelfTest[6];    // holds results of gyro and accelerometer self test

// global constants for 9 DoF fusion and AHRS (Attitude and Heading Reference System)
float GyroMeasError = PI * (40.0f / 180.0f);   // gyroscope measurement error in rads/s (start at 40 deg/s)
float GyroMeasDrift = PI * (0.0f  / 180.0f);   // gyroscope measurement drift in rad/s/s (start at 0.0 deg/s/s)
// There is a tradeoff in the beta parameter between accuracy and response speed.
// In the original Madgwick study, beta of 0.041 (corresponding to GyroMeasError of 2.7 degrees/s) was found to give optimal accuracy.
// However, with this value, the LSM9SD0 response time is about 10 seconds to a stable initial quaternion.
// Subsequent changes also require a longish lag time to a stable output, not fast enough for a quadcopter or robot car!
// By increasing beta (GyroMeasError) by about a factor of fifteen, the response time constant is reduced to ~2 sec
// I haven't noticed any reduction in solution accuracy. This is essentially the I coefficient in a PID control sense; 
// the bigger the feedback coefficient, the faster the solution converges, usually at the expense of accuracy. 
// In any case, this is the free parameter in the Madgwick filtering and fusion scheme.
float beta = sqrt(3.0f / 4.0f) * GyroMeasError;   // compute beta
float zeta = sqrt(3.0f / 4.0f) * GyroMeasDrift;   // compute zeta, the other free parameter in the Madgwick scheme usually set to a small or zero value
#define Kp 2.0f * 5.0f // these are the free parameters in the Mahony filter and fusion scheme, Kp for proportional feedback, Ki for integral
#define Ki 0.0f

uint32_t delt_t = 0; // used to control display output rate
uint32_t lastUpdate = 0, firstUpdate = 0; // used to calculate integration interval
uint32_t Now = 0;        // used to calculate integration interval

float ax, ay, az, gx, gy, gz, mx, my, mz; // variables to hold latest sensor data values 
float q[4] = {1.0f, 0.0f, 0.0f, 0.0f};    // vector to hold quaternion
float eInt[3] = {0.0f, 0.0f, 0.0f};       // vector to hold integral error for Mahony method


// BMP280 初始設定和數值宣告**********************************************************************************************************
Adafruit_BMP280 bmp; // I2C
//Adafruit_BMP280 bmp(BMP_CS); // hardware SPI
//Adafruit_BMP280 bmp(BMP_CS, BMP_MOSI, BMP_MISO,  BMP_SCK);
float BMP_Temp = 0;        // unit: 度C
float BMP_P = 0;           // unit: pa
float BMP_Height = 0;      // unit: meters
float init_height = 0;

// GPS 參數設定 **********************************************************************************************************
#include <TinyGPS.h>
TinyGPS gps;
boolean gps_flag = false;
float latitude, longitude;   // 緯度, 經度
unsigned long age;    // Fix  Date

// 超音波 Ultrasonic 參數設定 **********************************************************************************************************
# define Ultra_front_trig 22
# define Ultra_front_echo 23
# define Ultra_back_trig 24
# define Ultra_back_echo 25
# define Ultra_left_trig 26
# define Ultra_left_echo 27
# define Ultra_right_trig 28
# define Ultra_right_echo 29
# define Ultra_bottom_trig 30
# define Ultra_bottom_echo 31
float distance_front = 0;
float distance_back = 0;
float distance_left = 0;
float distance_right = 0;
float distance_bottom = 0;

// Channel宣告 **********************************************************************************************************
//init a Channel class to store and manage CannelX form reciever
# define ch_angle_pin 48
# define ch0_pin 49
# define ch1_pin 50
# define ch2_pin 51
# define ch3_pin 52
# define ch4_pin 53
Channel ch_angle;
Channel ch0;
Channel ch1;
Channel ch2;
Channel ch3;
Channel ch4;
int signal_angle, signal0, signal1, signal2, signal3, signal4;

// Brushless Motor宣告 **********************************************************************************************************
# define motor_w1_pin 8
# define motor_w2_pin 9
# define motor_r1_pin 10
# define motor_r2_pin 11
Servo motor_w1;
Servo motor_w2;
Servo motor_r1;
Servo motor_r2;
int base_w1 = 80;
int base_w2 = 65;
int base_r1 = 55;
int base_r2 = 70;

float info_w1, info_w2, info_r1, info_r2;

// Motion parameters宣告 **********************************************************************************************************
boolean Activate_flag = false;
boolean Back_flag = false;
boolean init_flag = false;
boolean angle_init = false;

// Fuzzy Controler宣告 **********************************************************************************************************
float drone_NM[11] = {1,0.9,0.8,0.7,0.6,0,0,0,0,0,0};
float drone_Z[11] = {0,0,0,0.2,0.5,1,0.5,0.2,0,0,0};
float drone_PM[11] = {0,0,0,0,0,0,0.6,0.7,0.8,0.9,1};
float roll_corr, pitch_corr, height_corr;
long cc;
// Setup**********************************************************************************************************
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial3.begin(9600);
  Serial.println("Ready to setup ...");
  pinMode(13,OUTPUT);
  BMP280_setup();
  Recieve_setup();
  Motor_setup();
//  Ultra_setup();
  MPU9250_setup();
  Init_angle(100);
  Serial.println("Setup OK!");
  digitalWrite(13, HIGH);
}


// Main Code**********************************************************************************************************
void loop() {
  Get_BMP_data();     // Temperature, Pressure, Height
  Get_GPS_data();     // Latitude, Longitude
  Get_MPU9250_data();  // Yaw, Pitch, Roll
  Signal_update();
  Angle_corr();
  Fuzzy_Out(roll, "Roll");
  Fuzzy_Out(pitch, "Pitch");
  Fuzzy_Out(BMP_Height, "Height");
  
  if(Activate_flag == true){
      if(init_flag == false){
        Serial.println("Motor initialize...");
        Motor_init();
        init_flag = true;
      }
//      if(angle_init == false){
//        Init_angle(100);
//      }
//    if(angle_init == true){
//      Motion_update();
//    }
    Motion_update();
  }
  else if(Activate_flag == false){
    info_w1 = 0;
    info_w2 = 0;
    info_r1 = 0;
    info_r2 = 0;
    Motor_update();
    init_flag = false;
    angle_init = false;
  }
  
  Print_data(500);   // print data to debug
  cc++;
}

void Print_data(int duration_time){
  static unsigned long past = millis();
  if(millis() - past > duration_time){
//    // Reciever R9DS signal print
//    Serial.print("ch0: "); Serial.print(signal0);
//    Serial.print(", ch1: "); Serial.print(signal1);
//    Serial.print(", ch2: ");Serial.print(signal2);
//    Serial.print(", ch3: ");Serial.print(signal3);
//    Serial.print(", ch4: ");Serial.print(signal4);
//    Serial.print(", ac_flag: ");Serial.print(Activate_flag);  // activate_flag
//    Serial.print(", bk_flag: ");Serial.println(Back_flag);  // back_flag

//  Motor information print
    Serial.print(info_w1); Serial.print(" , "); 
    Serial.print(info_w2); Serial.print(" , "); 
    Serial.print(info_r1); Serial.print(" , "); 
    Serial.println(info_r2);
    
//    // BMP280 print
//    Serial.print("Temp: "); Serial.print(BMP_Temp); Serial.print(" *C");
//    Serial.print(", Pressure: "); Serial.print(BMP_P); Serial.print(" Pa");
//    Serial.print(", Altitude: "); Serial.print(BMP_Height); Serial.println(" m");  // this should be adjusted to your local forcase


//    // GPS print
//    Serial.print("Position: ");
//    Serial.print("latitude: ");Serial.print(latitude);Serial.print(" ");
//    Serial.print("longitude: ");Serial.println(longitude);


    // MPU9250 print
    Serial.print("Yaw: "); Serial.print(yaw, 2);
    Serial.print(", Pitch: ");Serial.print(pitch, 2);
    Serial.print(", Roll: ");Serial.print(roll, 2);
    Serial.print(", cc: ");Serial.println(cc);

    // Ultrasonic print
//     Serial.print("front: "); Serial.print(distance_front,2);
//     Serial.print(" ,back: "); Serial.print(distance_back,2);
//     Serial.print(" ,left: "); Serial.print(distance_left,2);
//     Serial.print(" ,right: "); Serial.print(distance_right,2);
//     Serial.print(" ,bottom: "); Serial.println(distance_bottom,2);

//    Serial.print("fuzzy_pitch: "); Serial.print(pitch_corr);
//    Serial.print(" ,fuzzy_roll: "); Serial.print(roll_corr);
//    Serial.print(" ,fuzzy_height: "); Serial.println(height_corr);
    past = millis();
  }
}

