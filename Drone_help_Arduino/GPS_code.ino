/*
 * 選用 GPS --> Grove GPS
 * Reference Website: http://wiki.seeed.cc/Grove-GPS/
 * Library: https://github.com/mikalhart/TinyGPS
 * 腳位:
 * VCC : 5V
 * GND : GND
 * TX : RX
 * RX : TX
 * 
 */


void Get_GPS_data(){
  // put your main code here, to run repeatedly:
    if(Serial3.available()){
         while (Serial3.available()){
          char c = Serial3.read();
//          Serial.write(c); // uncomment this line if you want to see the GPS data flowing
          if (gps.encode(c)) // Did a new valid sentence come in?
            gps_flag = true;
        }
        Serial3.flush();
      }
      if(gps_flag){
        gps.f_get_position(&latitude, &longitude, &age);
        latitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : latitude, 6;
        longitude == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : longitude, 6;
        gps_flag = false;
      }    
}
