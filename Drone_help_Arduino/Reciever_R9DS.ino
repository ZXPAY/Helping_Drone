 /*
 * 美國手 
 * 
 * 接收器 Recieve Library:
 * https://github.com/kiuz/PPM-Signal-Reader-ARDUINO/blob/master/PPMrcIn/PPMrcIn.cpp
 * http://github.com/kiuz/Arduino-Statistic-Library
 * 
 */

//
//  ReadSignalFromRC.pde
//  Example for PPMrcIn Arduino Library
//
//  Created by Domenico Monaco on 20/11/2011
//  Copyright 2011 Domenico Monaco - domenico.monaco@kiuz.it
//
//  License: GNU v2
//

void Recieve_setup(){
  Serial.println("Reciever Setup");
  pinMode (ch_angle_pin, INPUT);
  ch_angle.init(1,ch_angle_pin);
  pinMode (ch0_pin, INPUT);
  ch0.init(1,ch0_pin);
  pinMode (ch1_pin, INPUT);
  ch1.init(1,ch1_pin);
  pinMode (ch2_pin, INPUT);
  ch2.init(1,ch2_pin);
  pinMode (ch3_pin, INPUT);
  ch3.init(1,ch3_pin);
  pinMode (ch4_pin, INPUT);
  ch4.init(1,ch4_pin);
}

void Signal_update(){
  ch_angle.readSignal();
  signal_angle = ch_angle.getSignal();
  delay(5);
  
  ch0.readSignal();
  signal0 = ch0.getSignal();
  delay(5);
  
  ch1.readSignal();
  signal1 = ch1.getSignal();
  delay(5);
  
  ch2.readSignal();
  signal2 = ch2.getSignal();
  delay(5);
  
  ch3.readSignal();
  signal3 = ch3.getSignal();
  delay(5);
  
  ch4.readSignal();
  signal4 = ch4.getSignal();
  delay(5);

  if(signal0 <= 1200 && signal0 > 900){
    Activate_flag = true;
    Back_flag = false;
  }
  else if((signal0 > 1200) && (signal0 < 1693)){
    Activate_flag = false;
    Back_flag = false;
  }
  else{
    Back_flag = true;
  }
  
//  if(signal_angle < 1100){
//      init_yaw = yaw;
//      init_roll = roll;
//      init_pitch = pitch;
//  }

}


  
