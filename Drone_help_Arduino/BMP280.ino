// BMP280 Address --> 0x76
/*
 * 腳位:
 * VCC : 3.3V
 * GND : GND
 * SDA : SDA
 * SCL : SCL
 * Library: https://github.com/adafruit/Adafruit_BMP280_Library/blob/master/Adafruit_BMP280.h
 * 
 */
/***************************************************************************
  This is a library for the BMP280 humidity, temperature & pressure sensor

  Designed specifically to work with the Adafruit BMEP280 Breakout 
  ----> http://www.adafruit.com/products/2651

  These sensors use I2C or SPI to communicate, 2 or 4 pins are required 
  to interface.

  Adafruit invests time and resources providing this open source code,
  please support Adafruit andopen-source hardware by purchasing products
  from Adafruit!

  Written by Limor Fried & Kevin Townsend for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution
 ***************************************************************************/

void BMP280_setup() {
  if (!bmp.begin()) {  
    Serial.println(F("Could not find a valid BMP280 sensor, check wiring!"));
    while (1);
  }
}

void Get_BMP_data() {
    BMP_Temp = bmp.readTemperature();
    BMP_P = bmp.readPressure();
    BMP_Height = bmp.readAltitude(1013.25);
}
