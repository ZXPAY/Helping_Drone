/*
 * Fuzzy control function
 */

void Fuzzy_Out(int data, String type){
  int i;
  float map_i, v_i, area, moment;
  float u_l, fuzzy_output;
  float u_Gas[11]={};

  // Rule 1
  u_l = 0;
  if((data > 0) && (data<=40)){
   u_l = (float)(data-0)/40;    // 歸屬度計算
  }
//  Serial.print("Rule 1:"); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, drone_PM[i]));
    }
  }
  
  // Rule 2   在 Z 位置
  u_l = 0;
  if((data >= -20) && (data <= 0)){
   u_l = (float)(data+20)/20;       // 歸屬度計算
  }
  if((data > 0) && (data <= 20)){
   u_l = (float)(20-data)/20;      // 歸屬度計算
  }
//  Serial.print("Rule 2:"); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, drone_Z[i]));
    }
  }
  
  // Rule 3
  u_l = 0;
  if((data >=-40) && (data<=0)){
   u_l = (float)(0-data)/40;       // 歸屬度計算
  }
//  Serial.print("Rule 3:"); Serial.println(u_l);
  if(u_l != 0){
    for(i=0;i<11;i++){
      u_Gas[i] = max(u_Gas[i], min(u_l, drone_NM[i]));
    }
  }

  // 解模糊化 Use 重心法
  area = 0.0;
  moment = 0.0;
  for(i=0;i<11;i++){
    map_i = u_Gas[i];
//    Serial.print("map_i:"); Serial.println(map_i);
    v_i = (-5+i);
    area += map_i;
    moment += map_i * v_i;
  }
//  Serial.print("area:"); Serial.print(area); Serial.print(", moment:"); Serial.print(moment);
  if(area == 0){     // 都沒有在 Rule 規則裡面
    fuzzy_output = 0;
  }
  else{
    fuzzy_output = moment/area;
    if(type == "Roll"){
      roll_corr = fuzzy_output*2;  // 放大
    }
    else if(type == "Pitch"){
      pitch_corr = fuzzy_output*2;
    }
    else if(type == "Height"){
      height_corr = fuzzy_output*2;
    }
    else{
      Serial.println("Fuzzy input type error !");
    }
  }
  
}
//****************************************************
void Fuzzy_corr(){
  if(roll >= 0){
    
  }
  else{
    
  }

  if(pitch >= 0){
    
  }
  else{
    
  }
  if(BMP_Height >= 0){
    
  }
  else{
    
  }
}

