# define Ultra_font_trig 22
# define Ultra_font_echo 23
float distance_font = 0;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(Ultra_font_trig, OUTPUT);
  pinMode(Ultra_font_echo, INPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  Get_Ultra_Font();
  Serial.println(distance_font);
  delay(100);
}

/*
 * 假設目前平均溫度 30度
 * 超音波速度: 340 + 0.6*30 = 349 m/s = 349*100/1000000 = 0.0349  cm/us
 * us per cm = 1/0.0349 = 28.65 us/cm
 * 設定最大量測距離 150 cm: 來回秒數為: 28.65*150*2 = 8595.99   --> 進位:8600
 * 距離換算: 時間乘上速度/2(來回) = duration*0.0349/2 = duration*0.01745
 */
void Get_Ultra_Font(){
  int duration;
  digitalWrite(Ultra_font_trig, HIGH);
  delayMicroseconds(1000);
  digitalWrite(Ultra_font_trig, LOW); 
  duration = pulseIn(Ultra_font_echo, HIGH, 8600); 
  distance_font = duration*0.01745;
  if(distance_font <= 0){
    distance_font = 150;
  }
}



