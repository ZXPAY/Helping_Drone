//
//  ReadSignalFromRC.pde
//  Example for PPMrcIn Arduino Library
//
//  Created by Domenico Monaco on 20/11/2011
//  Copyright 2011 Domenico Monaco - domenico.monaco@kiuz.it
//
//  License: GNU v2
//

#include <PPMrcIn.h>

// must be install into libraries http://github.com/kiuz/Arduino-Statistic-Library

#include <Statistic.h>

//init a Channel class to store and manage CannelX form reciever
Channel ch1;
Channel ch2;
Channel ch3;
Channel ch4;

void setup() {
  Serial.begin(9600);
 
  Serial.println("Ready:");

  pinMode (50, INPUT);
  ch1.init(1,50);
  pinMode (51, INPUT);
  ch2.init(1,51);
  pinMode (52, INPUT);
  ch3.init(1,52);
  pinMode (53, INPUT);
  ch4.init(1,53);
}

void loop() {
  unsigned long a=millis();
  delay(5);
  ch1.readSignal();
  Serial.print(ch1.getSignal());
  Serial.print(" , ");
  delay(5);
  ch2.readSignal();
  Serial.print(ch2.getSignal());
  Serial.print(" , ");
  delay(5);
  ch3.readSignal();
  Serial.print(ch3.getSignal());
  Serial.print(" , ");
  delay(5);
  ch4.readSignal();
  Serial.print(ch4.getSignal());
  Serial.print(" , ");
  Serial.println(millis()-a);
}
